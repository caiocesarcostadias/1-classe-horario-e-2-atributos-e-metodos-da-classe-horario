package horario;

public class Teste {

	public static void main(String[] args) {
		Horario horario = new Horario();
		System.out.println(horario);

		horario.setHora(23);
		horario.setMinuto(55);
		horario.setSegundo(15);

		System.out.println(horario);

		horario.incrementaHora();
		System.out.println(horario);
		horario.incrementaMinuto();
		System.out.println(horario);
		horario.incrementaSegundo();
		System.out.println(horario);

		System.out.println();

		for (int i = 0; i < 60; i++) {
			horario.incrementaSegundo();
			System.out.println(horario);
		}

		System.out.println();

		for (int i = 0; i < 60; i++) {
			horario.incrementaMinuto();
			System.out.println(horario);
		}

		System.out.println();

		for (int i = 0; i < 24; i++) {
			horario.incrementaHora();
			System.out.println(horario);
		}

	}

}
