package horario;

public class Horario {

	private byte hora;
	private byte minuto;
	private byte segundo;

	public Horario() {
		this(0, 0, 0);
	}

	public Horario(int hora, int minuto, int segundo) {
		setHora(hora);
		setMinuto(minuto);
		setSegundo(segundo);
	}

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		if (!(hora >= 0 && hora < 24)) {
			throw new IllegalArgumentException("Hora inválida!");
		}
		this.hora = (byte) hora;
	}

	public int getMinuto() {
		return minuto;
	}

	public void setMinuto(int minuto) {
		if (!(minuto >= 0 && minuto < 60)) {
			throw new IllegalArgumentException("Minuto inválido!");
		}
		this.minuto = (byte) minuto;
	}

	public int getSegundo() {
		return segundo;
	}

	public void setSegundo(int segundo) {
		if (!(segundo >= 0 && segundo < 60)) {
			throw new IllegalArgumentException("Segundo inválido!");
		}
		this.segundo = (byte) segundo;
	}

	public void incrementaHora() {
		byte aux = (byte) (hora + 1);

		if (aux == 24) {
			hora = 0;
		} else {
			hora = aux;
		}
	}

	public void incrementaMinuto() {
		byte aux = (byte) (minuto + 1);

		if (aux == 60) {
			minuto = 0;
			incrementaHora();
		} else {
			minuto = aux;
		}
	}

	public void incrementaSegundo() {
		byte aux = (byte) (segundo + 1);

		if (aux == 60) {
			segundo = 0;
			incrementaMinuto();
		} else {
			segundo = aux;
		}
	}

	@Override
	public String toString() {
		return String.format("%02d:%02d:%02d", getHora(), getMinuto(), getSegundo());
	}
}
